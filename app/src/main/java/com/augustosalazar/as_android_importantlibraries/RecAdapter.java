package com.augustosalazar.as_android_importantlibraries; /**
 * Created by Laboratorio on 18/04/2016.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

public class RecAdapter extends RecyclerView.Adapter<RecAdapter.MyViewHolder>{

    private final Context context;
    private LayoutInflater inflater;
    private List<String> data = Collections.emptyList();
    private th_HomepageRecListener mRecyclerClickListner;

    public RecAdapter(Context context, List<String> data){
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.rec_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        String name = data.get(position);
        holder.tv1.setText(name);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public void addItem(int position, String data) {
        this.data.add(position, data);
        notifyItemInserted(position);
    }

    public void editItem(int position, String data) {
        this.data.set(position, data);
        notifyItemChanged(position);
    }

    public void removeItem(int position) {
        this.data.remove(position);
        notifyItemRemoved(position);
    }

    public int getIndexOf(String data){
        int i = 0;
        for (String name : this.data){
            if(name.equals(data)){
                return i;
            }
            else{
               i++;
            }
        }
        return -1;
    }

    public String getItem(int position){return this.data.get(position);

    }

    public void clearData() {
        int size = this.data.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                this.data.remove(0);
            }

            this.notifyItemRangeRemoved(0, size);
        }
    }

    public void setRecyclerClickListner(th_HomepageRecListener recyclerClickListner){
        mRecyclerClickListner = recyclerClickListner;
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv1;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tv1 = (TextView) itemView.findViewById(R.id.tvRecValue);
        }

        @Override
        public void onClick(View v) {
            if (mRecyclerClickListner != null) {
                mRecyclerClickListner.itemClick(v, getAdapterPosition());
            }
        }
    }

    public interface th_HomepageRecListener
    {
        public void itemClick(View view, int position);
    }
}