package com.augustosalazar.as_android_importantlibraries;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Random;

import eventBus.MessageChangeF1;
import eventBus.MessageChangeF2;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2 extends Fragment {

    private EditText tvName;
    private EditText tvLastName;

    @Override
    public void onStart() {
        super.onStart();
        Log.d(General.TAG, "F2 On start");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment2, container, false);

        tvName     = (EditText)v.findViewById(R.id.tvName);
        tvLastName = (EditText)v.findViewById(R.id.tvLastName);

        Log.d(General.TAG, "F2 onCreateView");
        return v;
    }

    @Subscribe
    public void onMessageEvent(MessageChangeF2 event){
        Log.d(General.TAG, "F2 onMessageEvent "+event.message);
        String name = tvName.getText().toString();
        String lastName = tvLastName.getText().toString();
        FullName fullName = new FullName(name, lastName);
        fullName.save();
        EventBus.getDefault().post(new MessageChangeF1("Data saved to db"));
        tvName.setText("");
        tvLastName.setText("");
        tvName.requestFocus();
    }

}
