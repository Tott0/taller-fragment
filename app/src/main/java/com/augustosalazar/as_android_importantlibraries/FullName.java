package com.augustosalazar.as_android_importantlibraries;

import com.orm.SugarRecord;

/**
 * Created by Laboratorio on 16/05/2016.
 */
public class FullName extends SugarRecord {
    String name;
    String lastName;

    public FullName(){

    }

    public FullName(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }
}
