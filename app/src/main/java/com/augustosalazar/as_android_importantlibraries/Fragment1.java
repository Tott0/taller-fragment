package com.augustosalazar.as_android_importantlibraries;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import eventBus.MessageChangeF1;
import eventBus.MessageChangeF2;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment1 extends Fragment {

    private List<String> mData = new ArrayList<>();
    private RecyclerView mrV;
    private RecAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    public Fragment1() {

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(General.TAG, "F1 On start");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_fragment1, container, false);

        mrV = (RecyclerView) v.findViewById(R.id.recView);
        mrV.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mrV.setLayoutManager(mLayoutManager);
        mAdapter = new RecAdapter(getActivity(),mData);
        mrV.setAdapter(mAdapter);
        //mAdapter.setRecyclerClickListner(this);


        return v;
    }

    @Subscribe
    public void onMessageEvent(MessageChangeF1 event){
        Log.d(General.TAG, "F1 onMessageEvent "+event.message);
        FullName fullName = FullName.findById(FullName.class, FullName.count(FullName.class));
        String data = fullName.name + " " + fullName.lastName;
        mAdapter.addItem(mData.size(), data);
    }
}
